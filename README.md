# CCRPC Synthopop


## Population Synthesizer

This [example notebook](synthpop/demo.ipynb) illustrates an example of using synthpop to generate base year population.

1. On your local machine, clone this repo

2. Build a docker image. If you haven't got Docker, please [follow these installation instructions](https://docs.docker.com/install/)

``` bash
    cd ccrpc-synthpop
    docker build -t ccrpc_synthpop:latest .
```

3. Start a single container

``` bash
    docker run -p --rm 8888:8888 ccrpc_synthpop:latest
```

4. Run the notebook `demo.ipynb` inside the `synthpop` folder

## Credit

This application uses Open Source components. You can find the source code of their open source projects along with license information below. We acknowledge and are grateful to these developers for their contributions to open source.

Project: Synthpop [https://github.com/UDST/synthpop](https://github.com/UDST/synthpop)
Copyright 2016, UrbanSim Inc. All right reserved.
License (BSD 3-Clause "New" or "Revised")

Project: UrbanSim [https://github.com/UDST/urbansim](https://github.com/UDST/urbansim)
Copyright 2016, UrbanSim Inc. All right reserved.
License (BSD 3-Clause "New" or "Revised")

## License

CCRPC Synthpop is available under the terms of the
[BSD 3-clause license][2].

[1]: https://ccrpc.org/
[2]: https://gitlab.com/ccrpc/land-use-model/blob/master/LICENSE.md

