""" Python script to map households table variables. This script will be ran
    after sythpop generated synthetic population to accomodate the meaning of
    variables defined in the Urban-Sim tables.

    See project documentation on the wiki for more descriptions on variables
"""

import numpy as np
import pandas as pd

hh = pd.read_csv("households_table.csv", index_col="hh_id")
df_h = pd.DataFrame()
df_h["persons"] = hh.NP
df_h["income"] = hh.HINCP
conditions_tenure = [
    (hh["tenure_mover"] == "own not recent")
    | (hh["tenure_mover"] == "own recent"),
    (hh["tenure_mover"] == "rent not recent")
    | (hh["tenure_mover"] == "rent recent"),
]
choices_tenure = [1, 2]
df_h["tenure"] = np.select(conditions_tenure, choices_tenure)
df_h["serialno"] = hh.serialno
df_h["cars"] = hh.VEH
df_h["race_of_head"] = hh.race_of_head
df_h["age_of_head"] = hh.age_of_head
df_h["workers"] = hh.workers
df_h["children"] = hh.R18
conditions_mover = [(hh["MV"] <= 3), (hh["MV"] >= 4)]
choices_mover = [True, False]
df_h["recent_mover"] = np.select(conditions_mover, choices_mover, default=None)
df_h.to_csv("households_synth.csv")
