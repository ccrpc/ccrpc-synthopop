import os
from collections import namedtuple
import urllib.request
import pandas as pd
import json

os.environ["CENSUS_API_KEY"] = "INSERT_CENSUS_KEY_HERE"
acs_year = 2015

ACS = namedtuple("Field", ["name", "option"])
p_acs = {
    ACS(
        "person_age", "19 and under"
    ): "(B01001_003E + B01001_004E + B01001_005E + "
    "B01001_006E + B01001_007E + B01001_027E + "
    "B01001_028E + B01001_029E + B01001_030E + "
    "B01001_031E)",
    ACS(
        "person_age", "20 to 35"
    ): "(B01001_008E + B01001_009E + B01001_010E + "
    "B01001_011E + B01001_012E + B01001_032E + "
    "B01001_033E + B01001_034E + B01001_035E + "
    "B01001_036E)",
    ACS(
        "person_age", "35 to 60"
    ): "(B01001_013E + B01001_014E + B01001_015E + "
    "B01001_016E + B01001_017E + B01001_037E + "
    "B01001_038E + B01001_039E + B01001_040E + "
    "B01001_041E)",
    ACS(
        "person_age", "above 60"
    ): "(B01001_018E + B01001_019E + B01001_020E + "
    "B01001_021E + B01001_022E + B01001_023E + "
    "B01001_024E + B01001_025E + B01001_042E + "
    "B01001_043E + B01001_044E + B01001_045E + "
    "B01001_046E + B01001_047E + B01001_048E + "
    "B01001_049E)",
    ACS("race", "white"): "(B02001_002E)",
    ACS("race", "black"): "(B02001_003E)",
    ACS("race", "asian"): "(B02001_005E)",
    ACS("race", "other"): "(B02001_004E + B02001_006E + B02001_007E + "
    "B02001_008E)",
    ACS("person_sex", "male"): "(B01001_002E)",
    ACS("person_sex", "female"): "(B01001_026E)",
    ACS("hispanic", "yes"): "(B03003_003E)",
    ACS("hispanic", "no"): "(B03003_002E)",
    ACS("hh_population", "all"): "B11002_001E",
    ACS("population", "all"): "B01001_001E",
    ACS("hispanic", "all"): "B03003_002E + B03003_003E",
    ACS("sex", "all"): "B01001_002E + B01001_026E",
    ACS("race", "all"): " + ".join(["B02001_0%02dE" % i for i in range(2, 9)]),
    ACS("male_age_columns", "all"): " + ".join(
        ["B01001_0%02dE" % i for i in range(3, 26)]
    ),
    ACS("female_age_colums", "all"): " + ".join(
        ["B01001_0%02dE" % i for i in range(27, 50)]
    ),
}


h_acs = {
    ACS("sf_detached", "yes"): "B25032_003E + B25032_014E",
    ACS("sf_detached", "no"): "B25032_001E - B25032_003E - B25032_014E",
    ACS(
        "hh_age_of_head", "lt35"
    ): "B25007_003E + B25007_004E + B25007_013E + B25007_014E",
    ACS(
        "hh_age_of_head", "gt35-lt65"
    ): "B25007_005E + B25007_006E + B25007_007E + B25007_008E + "
    "B25007_015E + B25007_016E + B25007_017E + B25007_018E",
    ACS("hh_age_of_head", "gt65"): "B25007_009E + B25007_010E + B25007_011E + "
    "B25007_019E + B25007_020E + B25007_021E",
    ACS("hh_race_of_head", "black"): "B25006_003E",
    ACS("hh_race_of_head", "white"): "B25006_002E",
    ACS("hh_race_of_head", "asian"): "B25006_005E",
    ACS(
        "hh_race_of_head", "other"
    ): "B25006_004E + B25006_006E + B25006_007E + B25006_008E ",
    ACS("hispanic_head", "yes"): "B25003I_001E",
    ACS("hispanic_head", "no"): "B11005_001E - B25003I_001E",
    ACS("hh_children", "yes"): "B11005_002E",
    ACS("hh_children", "no"): "B11005_011E",
    ACS("seniors", "yes"): "B11007_002E",
    ACS("seniors", "no"): "B11007_007E",
    ACS("hh_income", "lt30"): "B19001_002E + B19001_003E + B19001_004E + "
    "B19001_005E + B19001_006E",
    ACS("hh_income", "gt30-lt60"): "B19001_007E + B19001_008E + B19001_009E + "
    "B19001_010E + B19001_011E",
    ACS("hh_income", "gt60-lt100"): "B19001_012E + B19001_013E",
    ACS("hh_income", "gt100-lt150"): "B19001_014E + B19001_015E",
    ACS("hh_income", "gt150"): "B19001_016E + B19001_017E",
    ACS("hh_cars", "none"): "B08201_002E",
    ACS("hh_cars", "one"): "B08201_003E",
    ACS("hh_cars", "two or more"): "B08201_004E + B08201_005E + B08201_006E",
    ACS("hh_workers", "none"): "B08202_002E",
    ACS("hh_workers", "one"): "B08202_003E",
    ACS("hh_workers", "two or more"): "B08202_004E + B08202_005E",
    ACS("tenure_mover", "own recent"): "B25038_003E",
    ACS("tenure_mover", "own not recent"): "B25038_002E - B25038_003E",
    ACS("tenure_mover", "rent recent"): "B25038_010E",
    ACS("tenure_mover", "rent not recent"): "B25038_009E - B25038_010E",
    ACS("hh_size", "one"): "B25009_003E + B25009_011E",
    ACS("hh_size", "two"): "B25009_004E + B25009_012E",
    ACS("hh_size", "three"): "B25009_005E + B25009_013E",
    ACS("hh_size", "four or more"): "B25009_006E + B25009_014E + "
    "B25009_007E + B25009_015E + "
    "B25009_008E + B25009_016E + "
    "B25009_009E + B25009_017E",
}

acs_choice = {"H": h_acs, "P": p_acs}
file_choice = {
    "H": f"{acs_year}_county19_synthpop_household.csv",
    "P": f"{acs_year}_county19_synthpop_people.csv",
}
select_file_choice = {
    "H": f"{acs_year}_county19_synthpop_household_selected.csv",
    "P": f"{acs_year}_county19_synthpop_people_selected.csv",
}

var_dict = acs_choice["H"]
file = file_choice["H"]
select_file = select_file_choice["H"]

variables = []
for key, val in var_dict.items():
    val = (
        val.replace("+", "-")
        .replace("*", "-")
        .replace("/", "-")
        .replace("(", "-")
        .replace(")", "-")
        .replace("1.0", "-")
        .split("-")
    )
    for v in val:
        v = v.strip(" ")
        if v and v not in variables:
            variables.append(v)


class census:
    geoid_dict = {}

    def bg_data_update(self, state="17"):
        """
        To get all counties use classmethod
        CensusBlockGroup.get_counties_queryset()
        """
        counties = ["019"]
        for county in counties:
            print(f"Updating county: {county}")
            vars_bundle = self._bundle_vars("block_group")

            for vars in vars_bundle:
                data = self._api_call(vars, county, state, "block_group")
                header = data[0]
                state_index = header.index("state")
                county_index = header.index("county")
                tract_index = header.index("tract")
                block_group_index = header.index("block group")
                # Construct Geoid: SSCCTTTTTTG,
                # S: State, C: County, T:Tract, G: Block Group
                variable_names = []
                for v_name in header[0:state_index]:
                    variable_names.append(v_name)
                # Create a dictionary from the data downloaded
                # geoid_dict = {'geoid_1': {'v1': val1, 'v2': val2, ...},
                #               'geoid_2': {'v1': val3, 'v3': val4, ...},
                #                ...
                #              }

                for d in data[1:]:
                    d_int = [self._convert2int(i) for i in d]
                    geoid = (
                        d[state_index]
                        + d[county_index]
                        + d[tract_index]
                        + d[block_group_index]
                    )
                    variables_dict = dict(
                        zip(variable_names, d_int[0:state_index])
                    )
                    if geoid in self.geoid_dict:
                        self.geoid_dict[geoid] = {
                            **self.geoid_dict[geoid],
                            **variables_dict,
                        }
                    else:
                        self.geoid_dict[geoid] = variables_dict
            #             self._save_to_db(geoid_dict, "block_group")
            print(self.geoid_dict)
            print(f"Completed county: {county}")

    def _convert2int(self, s):
        """ Helper function to convert non-null string to int """
        if not s or s == "null":
            return None
        return int(s)

    def _bundle_vars(self, option):
        """ Census API allows up to 50 variables in each call """
        chunk_size = 50
        if option == "block_group":
            vars = self._chunks(variables, chunk_size)
        elif option == "tract":
            vars = self._chunks(variables, chunk_size)
        else:
            print("Unknown geography")
        return vars

    def _chunks(self, l, n):
        """Yield successsive n-sized chunks,
        Note: Yield Expression will fail in Python 3.8+  """
        for i in range(0, len(l), n):
            yield l[i : i + n]

    def _api_call(self, vars, county, state, option):
        """ Retrieves data from census website for requested variables """
        vars_string = (",").join(vars)
        if option == "block_group":
            link = (
                f"https://api.census.gov/data/{acs_year}"
                f"/acs/acs5?get={vars_string}"
                f"&for=block%20group:*&in=state:{state}%20county:{county}"
                f"&key={os.environ.get('CENSUS_API_KEY')}"
            )
            print(link)
        elif option == "tract":
            link = (
                f"https://api.census.gov/data/{acs_year}"
                f"/acs/acs5?get={vars_string}"
                f"&for=tract:*&in=state:{state}%20county:{county}"
                f"&key={os.environ.get('CENSUS_API_KEY')}"
            )
        try:
            with urllib.request.urlopen(link) as url:
                data = json.loads(url.read().decode())
        except urllib.error.HTTPError as err:
            if err.code == 500:
                data = self._urlopen_retry(link)
            if err.code == 400:
                print("Bad request, maybe bad link", link)
                raise BadRequestError(link)
            else:
                pass
        return data

    def _urlopen_retry(self, link):
        MAX_RETRY = 5
        attempt = 0
        """Automatically retry after server error """
        while True:
            try:
                attempt += 1
                with urllib.request.urlopen(link) as url:
                    print("retrying", link)
                    return json.loads(url.read().decode())
            except urllib.error.HTTPError as err:
                if attempt >= MAX_RETRY:
                    raise MaximumRetriesError(link)
                if err.code == 500:
                    time.sleep(5)
                    pass
                else:
                    print("UnknownError", link)
                    pass


c = census()
c.bg_data_update()

result = {}

for bg, d in c.geoid_dict.items():
    result[bg] = {}
    for key, val in var_dict.items():
        var_name = key.name + ", " + key.option
        try:
            var_value = eval(val, d)
        except TypeError:
            var_value = None
        result[bg][var_name] = var_value

df = pd.DataFrame(result).T
df.to_csv(file)
df_select = df.loc[
    [
        "170190014001",
        "170190059003",
        "170190060001",
        "170190004011",
        "170190004012",
        "170190059002",
    ]
]
df_select.to_csv(select_file)


class Error(Exception):
    """Base class for other exceptions"""

    def __init__(self, link):
        self.link = link


class MaximumRetriesError(Error):
    """Raised when the maximum number of retries is exceeded """

    def __init__(self, link):
        super().__init__(link)
        print(f"Maximum number of retires is reached for {self.link}")


class BadRequestError(Error):
    """Raised when HTTP 400 Errors are detected"""

    def __init__(self, link):
        super().__init__(link)
        print(f"Bad request for {self.link}")
