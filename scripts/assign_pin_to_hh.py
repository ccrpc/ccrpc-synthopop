"""Script to assign pin number to each household based on total number
of household in a given parcel

    Preparation:
    create three folders in the same directory named:

    households, parcels, assigned

    Results:
    A new column "PIN" will be appended to the households table and be saved in
    the "assigned" folder. The file will have the same name as the household
    tables.

   """

import os
import pandas as pd
import numpy as np

p_files = [parcel for parcel in os.listdir("parcels") if ".csv" in parcel]
h_files = [
    household for household in os.listdir("households") if ".csv" in household
]
p_files.sort()
h_files.sort()
for i in range(len(p_files)):
    parcel = pd.read_csv(os.path.join(os.getcwd(), "parcels", p_files[i]))
    hh = pd.read_csv(os.path.join(os.getcwd(), "households", h_files[i]))
    parcel_id = []
    for index, row in parcel.iterrows():
        for j in range(row["Households"]):
            parcel_id.append(row["PIN"])
    hh["PIN"] = np.array(parcel_id)
    hh.to_csv(os.path.join(os.getcwd(), "assigned", h_files[i]))
