""" Python script to map persons table variables. This script will be ran after
    sythpop generated synthetic population to accomodate the meaning of
    variables defined in the Urban-Sim tables.

    See project documentation on the wiki for more descriptions on variables
"""
import numpy as np
import pandas as pd

ppl = pd.read_csv("persons_table.csv")
df_p = pd.DataFrame()
ppl.index.names = ["person_id"]
df_p["household_id"] = ppl.hh_id
df_p["member_id"] = ppl.SPORDER
df_p["age"] = ppl.AGEP
df_p["income"] = ppl.PERNP
df_p["education"] = ppl.SCHL
df_p["race_id"] = ppl.RAC1P
df_p["hh_relationship"] = ppl.RELP
df_p["sex"] = ppl.SEX

conditions_sch = [(ppl["SCH"] == 2) | (ppl["SCH"] == 3), (ppl["SCH"] == 1)]
choices_sch = [True, False]
df_p["student_status"] = np.select(conditions_sch, choices_sch, default=None)

conditions_work = [
    (ppl["SCH"] == 1)
    | (ppl["SCH"] == 2)
    | (ppl["SCH"] == 4)
    | (ppl["SCH"] == 5),
    (ppl["SCH"] == 3) | (ppl["SCH"] == 6),
]
choices_work = [True, False]
df_p["worker_status"] = np.select(conditions_work, choices_work, default=None)

df_p["hours"] = ppl.WKHP

conditions_wah = [
    (ppl["JWTR"] == 11),
    (ppl["JWTR"] <= 10) | (ppl["JWTR"] == 12),
]
choices_wah = [True, False]
df_p["work_at_home"] = np.select(conditions_wah, choices_wah, default=None)
df_p.to_csv("persons_synth.csv")
